function reduce(elements,cb,startingValue){
    var sum = 0;
        for (let i = 0; i < elements.length; i++) {
            sum += elements[i];
            if(startingValue !== undefined){
            sum = sum+startingValue ;
            }
            else {
                startingValue = elements[0]
            }
        }
        cb(sum)
    }
    
module.exports = reduce;
    